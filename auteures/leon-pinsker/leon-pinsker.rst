.. index::
   ! Léon Pinsker

.. _leon_pinsker:

=========================================================
**Léon Pinsker**
=========================================================

- https://en.wikipedia.org/wiki/Leon_Pinsker
- https://fr.wikipedia.org/wiki/L%C3%A9on_Pinsker


Biographie
=============

- https://fr.wikipedia.org/wiki/L%C3%A9on_Pinsker

Léon Pinsker, né en Pologne le 13 décembre 1821 et mort le 9 décembre 1891,
est un médecin et un militant sioniste qui milita toute sa vie pour
l'intégration des Juifs russes en créant notamment la Société pour la
promotion de l'instruction qui encourage l'apprentissage de la langue russe.

Après les pogroms de 1881, il écrivit anonymement et en langue allemande
à Berlin la brochure Autoémancipation (1882), dans laquelle il développait
certains des thèmes majeurs du sionisme naissant, dont la création
d'un État indépendant, mais pas forcément en Palestine, l'autre option
étant un territoire au niveau de l'Amérique du Nord.

Ce texte est ignoré par les Juifs occidentaux mais très bien accueilli
à l'est, où il est traduit en hébreu et yiddish.

Il y dénonce la judéophobie (« Judophobie » en allemand)2.

Il présida avec Moïse Lilienblum l'organisation des Amants de Sion
jusqu'en 1889.


Livres
========

- :ref:`auto_emancipation_1882`
