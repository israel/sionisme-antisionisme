.. index::
   ! Denis Charbit

.. _denis_charbit:

=========================================================
**Denis Charbit**
=========================================================

- :ref:`Denis Charbit <antisem:denis_charbit>`
- https://akadem.org/fiche_conferencier.php?id=363
- https://laviedesidees.fr/_Charbit-Denis

Présentation
==============

Denis Charbit a coordonné aussi une anthologie très intéressante de textes,
intintulée Sionisme(s). Textes fondamentaux (Albin-Michel, 1998), qui
montre la diversité du mouvement sioniste et les discussions qui l'ont
traversé dès le début.


Biographie
=============

- https://fr.wikipedia.org/wiki/L%C3%A9on_Pinsker

Denis Charbit est spécialiste du sionisme, professeur de sciences politiques
à la Faculté des Sciences humaines de l'Open University d’Israël (Raanana).

Il est également titulaire d’un doctorat de l’Université de Tel Aviv.

Ses champs de recherches recouvrent l'histoire intellectuelle de la
France au XXème siècle ainsi que l'histoire des idées et l'histoire
politique d'Israël, l'étude de la société et de la littérature israélienne.

Il est l'auteur, entre autres, de l’anthologie Sionismes, textes fondamentaux
(Albin Michel, 1998) ,

Qu’est-ce que le sionisme ? (Albin Michel, 2007) ou encore
dernièrement:Israël ; idées reçues (Le Cavalier bleu, 2015).

Proche des idées du mouvement “La paix maintenant”, il intervient
régulièrement dans les médias français sur la situation au Proche-Orient.

Discours 2024
===============

- :ref:`charbit_2024_02_29`

Livres
========

- :ref:`auto_emancipation_1882`
