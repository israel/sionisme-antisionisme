.. index::
   ! Auteures

.. _auteures:

=========================================================
**Auteur·es**
=========================================================

.. toctree::
   :maxdepth: 3

   denis-charbit/denis-charbit
   leon-pinsker/leon-pinsker
