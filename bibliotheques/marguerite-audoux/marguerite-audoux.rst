.. index::
   pair: Bibliothèque;  Marguerite Audoux
    ! Bibliothèque Marguerite Audoux

.. _bib_marguerite_audoux:

=========================================================
**Bibliothèque Marguerite Audoux**
=========================================================

- https://www.paris.fr/lieux/bibliotheque-marguerite-audoux-1665
- https://lestraverseesdumarais.com/BIBLIOTHEQUE-MARGUERITE-AUDOUX

Fermée du 24 octobre 2023 au 2 mars 2024
===========================================

.. figure:: images/fermeture_2023_2024.png

Adresse 10 rue Portefoin, Paris 3e
========================================

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3582705855369572%2C48.86315495288552%2C2.3618111014366154%2C48.864589484827796&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/48.86387/2.36004">Afficher une carte plus grande</a></small>


Livres à lire
==============

En plus de Pinsker et de “sionisme(s)”, je conseille de lire
Bernard Lazare, qui entame une réflexion sur le sionisme libertaire
dans “le fumier de Job”.

Tous ces livres ( et bien d’autres concernant l’histoire et les
cultures juives ) sont disponibles à l’emprunt à la bibliothèque
Marguerite Audoux ( actuellement en travaux mais qui devrait réouvrir
en mars 24 ).

