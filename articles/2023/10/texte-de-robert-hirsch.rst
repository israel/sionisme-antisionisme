.. index::
   pair: Robert Hirsch ; Sionisme (2023-10)

.. _robert_hirsch_2023_10_sionisme:

=================================================================================
2023-10 **Du sionisme et de l’antisionisme** par Robert Hirsch |RobertHirsch|
=================================================================================

- :ref:`antisem:robert_hirsch`

Introduction
==================================

Du sionisme et de l’antisionisme.

Ces termes sont sans cesse utilisés aujourd’hui sans que ceux
qui les utilisent sachent toujours de manière précise ce qu’ils
recouvrent et ce pourquoi ils et elles les utilisent.

Un rappel historique semble donc s’imposer, aussi pour s’interroger sur
les rapports complexes entre antisionisme et antisémitisme.

Car les choses ne sont pas simples : si l’antisionisme ne peut être
considéré comme intrinsèquement antisémite, il serait tout aussi faux
de nier toute relation, particulièrement aujourd’hui, entre antisionisme
et antisémitisme.

Aux origines du sionisme et de l’antisionisme
===============================================

Le sionisme est né comme mouvement politique à la fin du XIXème siècle
en réaction à la poussée antisémite qui caractérise ce moment historique
dans l’ensemble de l’Europe.

Une poussée marquée entre autres par les pogroms russes de 1882 et
l’Affaire Dreyfus en France (1894-1898), mais aussi par la création en
Allemagne du mot lui-même (1879) et de ligues et partis antisémites,
ainsi que par le passage à la mairie de Vienne d’un antisémite entre 1897 et 1910.

L’antisémitisme se développe à la fois dans les pays où
les Juifs ont obtenu l’égalité des droits et dans ceux où
l’émancipation politique n’a pas eu lieu.

Pour y faire face, Herzl, longtemps favorable à l’assimilation, mais
qui en doute désormais, écrit en 1896 "L’Etat des Juifs", ouvrage de
référence du sionisme, qui précède d’un an le Congrès fondateur du mouvement.

L’idée est de construire en Palestine un foyer national juif qui constituerait
un refuge pour les Juif/ves persécuté.es du monde entier.
Jusqu’à la Seconde Guerre mondiale, ce mouvement se heurte à quatre types
d’oppositions au sein du monde juif :

- La majorité des membres de la bourgeoisie juive s’inquiète d’un mouvement
  qui pourrait déstabiliser son intégration.
  C’est notamment vrai en France, Grande Bretagne et Allemagne.
  Par exemple, la bourgeoisie juive de Munich s’oppose à la tenue du premier congrès
  sioniste, qui aura finalement lieu à Bâle.
- Une grande partie des religieux juifs s’opposent fermement au sionisme,
  persuadés que c’est la volonté divine, et non celle des humains qui doit
  ramener les Juifs en Palestine.
  A leurs yeux, le sionisme, en tant que mouvement séculier, est un sacrilège.
- La majorité du mouvement ouvrier juif, notamment le Bund, est favorable
  à l’émancipation sociale et culturelle des Juif/ves là où ils et elles vivent.
  Les Juif/ves qui sont partie prenante du mouvement communiste après
  la Révolution russe rejettent également le sionisme, considéré
  comme un mouvement colonial.
- Des intellectuels judéo-allemands comme Hermann Cohen ou Franz Rosenzweig,
  qui refusent ce qu’ils considèrent comme une assimilation "au carré".
  Ils critiquent en effet dans le sionisme une volonté de "normaliser"
  le peuple juif, en le faisant devenir "une nation parmi les nations".
  Il s’agit là d’un courant plus minoritaire que les précédents.

Pour autant, si les résistances sont fortes, le sionisme n’est pas une
toute petite minorité comme le présentent souvent certains à gauche.

S’il est faible en France, par exemple, il a une influence de masse en
Russie, ainsi qu’en Roumanie dès le début du XXème siècle.

Il progresse après la Première Guerre mondiale au sein du judaïsme polonais.
Son influence est surtout importante dans les milieux populaires.

Ce sionisme naissant, profondément divisé, a pour contexte l’essor des
mouvements nationaux.
Comme eux, il se situe dans un moment de renaissance culturelle juive,
marquée par la référence à l’histoire, une référence pas toujours fondée
scientifiquement.
Mais la particularité du sionisme c’est qu’il est le fait d’un
"peuple sans terre", comme le mouvement l’affirme.
Et il se propose d’intégrer ce qu’il appelle "une terre sans peuple".

C’est là l’origine du drame : cette terre a un peuple
========================================================

C’est là l’origine du drame : cette terre a un peuple, les Palestiniens
et Palestiniennes, à la conscience nationale encore embryonnaire, mais
réelle.

Et cela, le sionisme veut très majoritairement l’ignorer.

Rappelons que la plupart des mouvements politiques, y compris les
socialistes, n’ont alors aucune considération pour les peuples coloniaux,
considérant même le plus souvent qu’il convient de leur "apporter la civilisation".

Le sionisme ne déroge pas à cette règle.

S’il s’agit donc d’un mouvement de libération nationale, structuré lors
de son développement en Palestine autour d’un idéal socialiste, son idéologie
et ses pratiques empruntent également au registre colonial européen,
ce qui fait toute sa complexité et sa singularité.

Le XXème siècle et les évolutions du sionisme et de l’antisionisme
======================================================================

Sans s’appesantir sur ce point, on peut rappeler ici les évolutions du
XXème siècle, dramatiques pour les Juif/ves.

La crise économique des années trente aux États-Unis et en Europe
relancèrent un antisémitisme seulement assoupi après les moments
antisémites de la fin du siècle précédent.

**La montée des fascismes fut accompagnée d’un retour au premier plan de la
haine des Juif/ves**.

**Et l’on sait que tout cela déboucha sur la Shoah, dont il est toujours
utile de rappeler ce qu’elle représenta comme destruction du monde juif
européen**.

Si les nazis perdirent la guerre, ils avaient largement réussi leur œuvre
exterminatrice.
Cette extermination mit fin aux espoirs émancipateurs du Bund, assassiné
avec le peuple juif du Yiddishland.
Dans le même temps, le rêve socialiste, un temps incarné par la Révolution
d’Octobre, sombrait dans le cauchemar stalinien et son antisémitisme.

L’avenir des Juif/ves passait désormais par le foyer national en train de devenir un Etat
=============================================================================================

L’avenir des Juif/ves passait désormais par le foyer national en train
de devenir un Etat.

Dès les années vingt, ce foyer juif en Palestine s’était confronté au
nationalisme palestinien, lequel vit dans l’arrivée des Juif/ves une forme
de colonialisme.

Cette réaction se doubla dès les premiers affrontements d’un antisémitisme
alimenté par les Frères musulmans, diffuseurs des Protocoles des sages de
Sion.

Une aile droite, très violente et hostile aux Palestiniens (l’Irgoun, et
son pendant politique, le sionisme révisionniste), se forma alors au
sein d’un mouvement sioniste encore dominé par la gauche travailliste.

Il y eut aussi des groupes sionistes cherchant une issue pacifique
(comme Martin Buber refusant "l’homogénéité ethnique").

Du début du XXème siècle à la Seconde Guerre mondiale, les diverses
alyas rassemblèrent des Juives et Juifs en butte à l’antisémitisme, notamment
celles et ceux venu.es d’Allemagne après l’accès au pouvoir d’Hitler en 1933.

En 1939, sous la pression du nationalisme arabe, les Anglais limitèrent
les entrées juives, montrant ainsi que l’impérialisme n’était pas
forcément toujours du côté du sionisme.


L’existence d’un Etat juif revendiquée par le mouvement sioniste à partir de 1942 représentait un refuge, voire une garantie contre le retour de la haine
============================================================================================================================================================

Après la guerre et l’extermination, **il apparut à la majorité des Juif/ves
dans le monde, qu’ils aillent (souvent parce qu’ils et elles n’avaient
plus où aller) ou pas en Palestine, que l’existence d’un Etat juif
(revendiquée par le mouvement sioniste à partir de 1942) représentait un refuge,
voire une garantie contre le retour de la haine**.

**L’effondrement des espoirs internationalistes laissait la place chez les
Juif/ves à une solution nationale**.

Mais cet État, né de la décision de l’ONU de novembre 1947, se mit en place en créant
une injustice envers les Palestinien.nes, qui fuirent ou furent expulsé.es
massivement.

La création de l’État d’Israël provoqua une évolution dans l’antisionisme,
désormais porté massivement par le monde arabe et par les pays du Sud en
train de se décoloniser.

L’Union soviétique développa un antisionisme  qui n’était que la couverture d’un antisémitisme stalinien popularisé
========================================================================================================================

L’Union soviétique lui emboîta le pas et, pour des raisons internes,
développa en son sein et dans les "démocraties populaires" **un antisionisme
qui n’était que la couverture d’un antisémitisme stalinien popularisé
dans le monde par les services de propagande du Kremlin**.

**Le terme "sioniste" servait à ne pas dire "juif"**.

Cette manipulation a encore aujourd’hui un succès non négligeable dans
certains milieux.

Dans le même temps, la nouvelle extrême gauche des années 68 retrouvait
les accents du vieil antisionisme de gauche (parfois mélangés avec des
scories staliniennes).

Mais, contrairement au début du XXème siècle, où cet antisionisme pouvait
se concevoir, il était désormais en décalage avec la réalité puisqu’une
nation juive israélienne s’était bel et bien constituée.

Une nation juive israélienne, un peuple palestinien injustement traité
=========================================================================

Depuis 1948, s’est en effet construite une nation juive, avec des
caractéristiques neuves, différentes de celles qui prévalaient dans la
diaspora.

Une idée force du sionisme réside dans la conviction que la puissance
israélienne permettra d’éviter ce que fut l’histoire souvent dramatique
des Juif/ves.

En même temps que la conviction de rompre avec le passé d’oppression
du peuple juif, les Israélien.nes ont gardé de leurs origines, et notamment
de la mémoire de la Shoah, la hantise de la destruction.

Les événements du 7 octobre 2023 et des jours suivants ont réveillé avec
force cette hantise en renvoyant la population israélienne au malheur juif.

La nation juive israélienne s’est trouvée depuis l’origine confrontée à
la question palestinienne, aggravée par les conquêtes de 1967.

Elle a pensé, dans sa majorité, que ce problème allait s’évanouir avec le
temps.
Mais ce n’est pas le cas et le peuple palestinien continue de réclamer
la justice, avec des aléas dans la représentation politique qu’il s’est
donné aux diverses périodes.

On le voit aujourd’hui dramatiquement avec le poids pris par le Hamas.

De plus, après une longue période où la social-démocratie était dominante,
sans que cela fasse avancer les rapports avec les Palestinien.nes, la
société israélienne, comme c’est le cas dans beaucoup de pays, a évolué
vers la droite.

L’occupation de territoires après 1967 au mépris des droits des populations,
ainsi que les rapports de domination exercés par les ashkenazes sur les
sépharades arrivé.es souvent plus tard, ont renforcé cette évolution.

Jusqu’à ce dramatique gouvernement actuel, avec des ministres d’extrême droite.

L’échec des accords d’Oslo a pesé négativement, bien entendu.

Après ce rapide survol historique, voyons comment envisager les notions
de sionisme et d’antisionisme aujourd’hui.

Que signifie être antisioniste aujourd’hui ?
===================================================

La droite et une partie de la gauche considèrent
que l’antisionisme d’aujourd’hui équivaut à de
l’antisémitisme.

C’est une vision simpliste, qui sert à
une utilisation politicienne contre une partie de la gauche.

Mais cette vision se trouve nourrie par les dérives d’un antisionisme
peu maîtrisé, qu’il convient d’interroger.

Un antisionisme qui n’évite pas toujours de recopier le stalinisme des années
cinquante, nommant "sionistes" les Juif/ves.

Le sionisme était le mouvement politique qui visait à constituer un foyer,
puis un État pour les Juif/ves, foyer puis État conçus comme refuges pour
les Juif/ves persécuté.es.

L’antisionisme s’y opposait, et pour celui du Bund, avançait une autre proposition,
celle de l’émancipation socialiste basée sur le respect de la culture
nationale juive.

Or, aujourd’hui, l’État existe, même s’il demeure menacé, les événements
récents le confirment.
Et les propositions internationalistes ont malheureusement échoué,
notamment en ce qui concerne les Juif/ves.


Être antisioniste aujourd’hui, qu’est-ce que cela veut dire ?
=================================================================

Être antisioniste aujourd’hui, qu’est-ce que cela veut dire ?

S’il s’agit de s’opposer aux politiques israéliennes, pourquoi pas ?

Mais ce n’est plus vraiment de l’antisionisme si cela ne remet pas en
cause l’existence de cet État, dont le fonctionnement peut parfaitement
être critiqué, comme c’est le cas de tous les États dans le monde.

Il est ainsi parfaitement légitime de remettre en cause une loi comme
celle de juillet 2018 sur "l’État nation du peuple juif".
Cela n’est pas spécifiquement "antisioniste", c’est simplement critiquer
une violation de l’État de droit et ses conséquences terribles
pour les citoyen.nes israélien.nes non juif/ves.

Un antisionisme cohérent existe, c’est celui des courants qui remettent en
cause l’existence de l’État d’Israël.
A cet égard, une formulation comme "libération de la Palestine", revendiquée
par le Hamas et **reprise par Danièle Obono**, pose problème.

Il ne s’agit plus alors de dénoncer l’occupation des territoires annexés
en 1967, mais de remettre en cause l’ensemble du pays, considéré comme
une colonie.

Certain.es argumentent d’ailleurs que le Hamas se serait attaqué le 7 octobre 2023, non
à des Juif/ves, mais à des colons.

Au-delà du fait que les enfants ne peuvent guère être considérés comme
des colons, cette formulation sous-entend que tout Israël n’est qu’un
territoire colonial que les habitants devraient quitter en cas de
"décolonisation".

**On voit bien l’inanité d’un tel raisonnement**, pourtant existant dans
certains secteurs de la gauche.

Comme l’ont rappelé récemment nos ami.es, les Juives et Juifs Révolutionnaires,
"aujourd’hui, la plupart des israélien·nes sont des sabra, né·es en Israël,
c’est leur pays et ils et elles n’en ont pas d’autres".

Pour éviter les ambiguïtés, il convient à nos yeux que la référence à l’antisionisme soit abandonnée
=======================================================================================================

Pour éviter les ambiguïtés, **il convient à nos yeux que la référence à
l’antisionisme soit abandonnée**.

**L’antisionisme pouvait être légitime avant la création de l’État d’Israël,
il ne peut plus l’être aujourd’hui**.

Il devient même une **référence dangereuse**, mettant en *exception l’État d’Israël,
le seul au monde dont il faudrait obtenir la disparition*.

Renoncer à ce terme ne peut à nos yeux que favoriser l’action en faveur
d’une solution qui respecte les deux peuples.

A gauche, il convient d’en discuter en abordant le fond, notamment historique,
de la question.

**Le RAAR est prêt à contribuer à ce débat, devenu indispensable**.





