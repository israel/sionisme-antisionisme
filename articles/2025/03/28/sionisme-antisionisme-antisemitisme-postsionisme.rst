.. index::
   ! Sionisme, antisionisme, antisémitisme, postsionisme (2025-03-28)
   pair: Sylvaine Bulle ; Sionisme, antisionisme, antisémitisme, postsionisme (2025-03-28)

.. _sylvaine_bulle_2025_03_28:

========================================================================
2025-03-28 **Sionisme, antisionisme, antisémitisme, postsionisme**
========================================================================

- http://www.grand-angle-libertaire.net/etape-explorations-theoriques-anarchistes-pragmatistes-pour-lemancipation/
- http://www.atelierdecreationlibertaire.com/Repenser-l-Etat-au-XXIe-siecle.html

Description
==============

Sionisme, antisionisme, antisémitisme, postsionisme", débat entre :

- Nathan Delbrassine, "Arguments antisionistes"
- et Sylvaine Bulle, "Critique de l’antisionisme et postsionisme"

Nathan Delbrassine et Sylvaine Bulle sont des participants réguliers du `séminaire ETAPE <http://www.grand-angle-libertaire.net/etape-explorations-theoriques-anarchistes-pragmatistes-pour-lemancipation/>`_.

Animé par Guy Lagrange

Nathan Delbrassine
====================

- http://www.atelierdecreationlibertaire.com/Repenser-l-Etat-au-XXIe-siecle.html

Nathan Delbrassine est diplômé en droit international et en langues orientales, 
doctorant en sociologie en cotutelle entre l’Université de Liège (Belgique) 
et l’Université Paris Cité. 

Il a participé à l’occupation en solidarité avec la Palestine de l’Université libre 
de Bruxelles en mai-juin 2024. 

Il est notamment l’auteur de "La culture comme vecteur de construction et de 
différenciation de l’État : vers un État d’émancipation ?" 
(dans Séminaire ETAPE, `Repenser l’État au XXIe siècle. Libertaires et pensées critiques, 
Atelier de création libertaire, 2023 <http://www.atelierdecreationlibertaire.com/Repenser-l-Etat-au-XXIe-siecle.html>`_).

**Sylvaine Bulle**
====================

- https://aoc.media/analyse/2023/10/10/le-hamas-et-le-kibboutz/
- https://aoc.media/opinion/2024/03/10/israel-gaza-contre-le-campisme/
- https://k-larevue.com/malm-antisemitisme-vert/
- :ref:`antisem:sylvaine_bulle`

Sylvaine Bulle est professeure de sociologie à l’Université Paris Cité, détachée 
au Laboratoire d’anthropologie politique (EHESS-CNRS). 

Elle a mené des recherches en Israël et en Palestine. 

Elle est l’autrice notamment de :

- `Le Hamas et le kibboutz <https://aoc.media/analyse/2023/10/10/le-hamas-et-le-kibboutz/>`_, site culturel AOC, 11 octobre 2023 [site sur abonnement mais trois textes gratuits par mois si on donne son email]
- `Israël-Gaza : contre le campisme <https://aoc.media/opinion/2024/03/10/israel-gaza-contre-le-campisme/>`_, site culturel AOC, 11 mars 2024
- `Andreas Malm et l’antisémitisme vert <https://k-larevue.com/malm-antisemitisme-vert/>`_, site de la revue K, 11 septembre 2024



Guy Lagrange 
================

Guy Lagrange est militant de la Fédération Anarchiste et coanimateur du séminaire ETAPE
