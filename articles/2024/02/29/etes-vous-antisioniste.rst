.. index::
   pair: Charbit; Etes-vous-antisioniste ? (2024-02-29)

.. _charbit_2024_02_29:

===========================================================
2024-02-29 **Etes-vous-antisioniste ?** par Denis Charbit
===========================================================

- https://akadem.org/etes-vous-antisioniste
- :ref:`denis_charbit`

Denis Charbit, professeur de sciences politiques
=====================================================

Denis Charbit est spécialiste du sionisme, professeur de sciences politiques à
la Faculté des Sciences humaines de l'Open University d’Israël (Raanana).
Il est également titulaire d’un doctorat de l’Université de Tel Aviv.

Ses champs de recherches recouvrent l'histoire intellectuelle de la France
au XXème siècle ainsi que l'histoire des idées et l'histoire politique
d'Israël, l'étude de la société et de la littérature israélienne.

Il est l'auteur, entre autres, de l’anthologie Sionismes, textes fondamentaux
(Albin Michel, 1998) , Qu’est-ce que le sionisme ? (Albin Michel, 2007) ou
encore dernièrement:Israël ; idées reçues (Le Cavalier bleu, 2015).

Proche des idées du mouvement “La paix maintenant”, il intervient régulièrement
dans les médias français sur la situation au Proche-Orient
