.. index::
   pair: Livre ; Qu'est-ce que le sionisme ? (de Denis Charbit, 2007)

.. _quoi_sionisme_2007:

=========================================================
**Qu'est-ce que le sionisme ?** de Denis Charbit
=========================================================

- https://www.babelio.com/livres/Charbit-Quest-ce-que-le-sionisme-/335541

Résumé
==========

De façon synthétique et thématique plutôt que chronologique, l'auteur
étudie le sionisme en tant que projet multiforme (retrouver la terre ancestrale,
reconstituer les juifs en nation, créer un état démocratique...) et
confronte son histoire aux arguments de ceux qui la contestent.

Quatrième de couverture
----------------------------

Le mot « sionisme », né il y a à peine plus de cent ans ne semble plus
compris de nos jours et subit toutes les défigurations qu'entraîne la polémique.

Ce livre vise à rétablir son sens véritable et à examiner ce qu'il en est
dans la réalité israélienne d'aujourd'hui : revenir à Sion, terre
ancestrale, reconstituer les juifs en tant que nation, créer un État démocratique,
rétablir la langue hébraïque et offrir aux juifs en détresse un refuge
et une patrie.

« Un petit livre lumineux, passionnant, documenté, ouvert et d’une grande
 honnêteté intellectuelle. » Bruno Frappat, La Croix.
