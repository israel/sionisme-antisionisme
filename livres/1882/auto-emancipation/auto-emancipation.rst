.. index::
   pair: Livre ; Auto-émancipation (de Leon Pinsker, 1882)

.. _auto_emancipation_1882:

=========================================================
**Auto-émancipation** de Leon Pinsker (1882)
=========================================================

- :ref:`leon_pinsker`
- https://www.jewishvirtuallibrary.org/quot-auto-emancipation-quot-leon-pinsker

Livre en français
====================

- https://fr.wikipedia.org/wiki/L%C3%A9on_Pinsker#Bibliographie

Léon Pinsker, Autoémancipation ! Avertissement d'un Juif russe à ses frères,
traduit de l'allemand par André Néher, Paris, 1882, rédité par Mille et
Une Nuits, Paris, 2006, préface de Georges Bensoussan.


Résumé wikipedia en anglais
===============================

- https://en.wikipedia.org/wiki/Auto-Emancipation

Auto-Emancipation (Selbstemanzipation) is a pamphlet written in German
by Russian-Polish Jewish doctor and activist Leon Pinsker in 1882.

It is considered a founding document of modern Jewish nationalism,
especially Zionism.

Pinsker discussed the origins of antisemitism and argued for Jewish
self-rule and the development of a Jewish national consciousness.

He wrote that Jews would never be the social equals of non-Jews until
they had a state of their own.
He called on Jewish leaders to convene and address the problem.

In the pamphlet, he describes anti-Jewish attacks as a psychosis, a
pathological disorder and an irrational phobia


Résumé de l'IREL
===================

- https://irel.ephe.psl.eu/ressources-pedagogiques/comptes-rendus-ouvrages/autoemancipation-avertissement-dun-juif-russe-a-ses

C'est parce qu’il a été profondément marqué par l'impact des pogroms
de 1881-1882 que Léon Pinsker a publié ce pamphlet, ensuite appelé à
faire date dans l'histoire du nationalisme juif.

Médecin juif russe, exerçant à Odessa, Léon Pinsker était, avant
1882, une figure des Lumières juives. En 1863, à Saint-Pétersbourg,
il faisait partie des fondateurs de la « Société pour la propagation
des Lumières » et il milita ainsi durant vingt ans pour l'intégration
politique, économique et sociale des Juifs en Russie.

Déjà ébranlé par le pogrom d'Odessa en 1871, les événements de 1881
le conduisirent à abandonner tout espoir d'amélioration de la situation
juive en Russie.

Il renonça notamment à combattre l'antisémitisme,
l'associant désormais à une maladie mentale : « La judéophobie est
une psychose. En tant que psychose, elle est héréditaire, en tant que
maladie transmise depuis deux mille ans, elle est incurable. »

Certes marquée par les dogmes de la médecine du XIXe siècle, sa théorie n'en
était pas moins absolument nouvelle.

Comme le « peuple juif est le peuple élu de la haine universelle », il ne
pouvait espérer trouver un foyer accueillant.

Partout, même au sein des nations libérales, il restait un étranger.

Le peuple juif était un peuple de « gueux », « indigne » et « méprisable ».
Seule une résurrection politique, une renaissance nationale serait en
mesure de restaurer la place du peuple juif au sein « de l'assemblée des
nations vivantes ».

Comment cependant s'y prendre ? Certes, le XIXe siècle a été celui des
restaurations nationales, mais « bien sûr, ils n'étaient pas juifs les
bienheureux qui obtinrent l'indépendance nationale. Ils demeuraient sur
leur propre sol et parlaient une seule langue ». Et Pinsker de conclure :
« En cela, il faut le reconnaître, ils l'emportaient sur nous. »

Pinsker ne se tourna pourtant pas inévitablement vers la Palestine.

Certes, il s'agit d'une option évidente, mais il n'écartait pas non plus la
perspective de l'Amérique qui avait déjà accueilli un nombre important
d'immigrés juifs.

À l'instar d’Herzl quelques années plus tard, Pinsker attendit une
reconnaissance diplomatique de la nécessité pour les Juifs d'obtenir un
foyer national.

Il comptait en cela beaucoup sur le judaïsme d'Europe
occidentale, notamment sur l'Alliance israélite universelle et sur son
pendant anglais l'Anglo Jewish Association qu'il somma d'abandonner la
bienfaisance pour l’action politique.

Ayant pour objectif de persuader
les élites israélites occidentales, Pinsker choisit de publier son
pamphlet en allemand. Il n'en tira quasiment aucun bénéfice.

En revanche, **l'ouvrage, aussitôt traduit en yiddish et en hébreu, connut un succès
certain auprès des cercles éclairés du judaïsme russe et galicien**.

Ce pamphlet, porté par la colère et la conviction nouvelle de son auteur,
constitue une pierre angulaire de la littérature nationaliste juive.

